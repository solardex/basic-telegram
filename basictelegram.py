#!/usr/bin/python3

import requests

class TelegramApi:
    telegram_token = ""

    def __init__(self, token):
        self.telegram_token = token

    def send_message(self, chat_id, msg):
        url = "https://api.telegram.org/bot" + self.telegram_token + "/sendMessage?chat_id=" + chat_id + "&parse_mode=html&text=" + msg
        return(requests.get(url).json()["ok"])
