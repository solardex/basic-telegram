#!/usr/bin/python3

from basictelegram import TelegramApi

token = input("token: ")
chat_id = input("chat_id: ")
msg = input("message: ")

print("sending...")
tele = TelegramApi(token)
tele.send_message(chat_id, msg)
print("done.")
